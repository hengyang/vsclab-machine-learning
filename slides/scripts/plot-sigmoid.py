#!/usr/bin/env python3

import sys

import numpy as np

from matplotlib import rc as pltrc
from matplotlib import pyplot as plt

def Main():
    pltrc('text', usetex=True)
    pltrc('font', family='serif')

    plt.figure(figsize=(6, 2))
    plt.subplots_adjust(top=0.95, left=0.08, right=0.98, bottom=0.15)
    x = np.linspace(-8, 8, num=100)

    plt.plot(x, 1 / (1 + np.exp(-x)))
    plt.xlim(-8, 8)
    plt.ylim(-0.1, 1.1)
    plt.grid(linestyle='--')
    plt.show()

if __name__ == '__main__':
    sys.exit(Main());
