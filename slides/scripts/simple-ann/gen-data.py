#!/usr/bin/env python3

import sys

import numpy as np

from matplotlib import rc as pltrc
from matplotlib import pyplot as plt

def Gen2DCircle(center, radius, one_sigma_thickness, n_dots, label):

    data = []
    for _ in range(n_dots):
        angle = np.random.rand() * np.pi * 2
        offset = np.random.randn() * one_sigma_thickness + radius

        new_item = [center[0] + np.cos(angle) * offset,
                    center[1] + np.sin(angle) * offset] + label
        data.append(new_item)

    return np.array(data)

def Main():

    data_a = Gen2DCircle(np.array([0, 0]), 0, 1.8, 400, [0])
    data_b = Gen2DCircle(np.array([0, 0]), 5, 0.8, 400, [1])

    with open("2-class-data.csv", "wb") as sink:
        for data in [data_a, data_b]:
            np.savetxt(sink, data, fmt='%.3f', delimiter=',')

    return 0

    pltrc('text', usetex=True)
    pltrc('font', family='serif')

    plt.figure(figsize=(4, 4))

    plt.axes().set_aspect('equal', 'datalim')
    plt.scatter(data_a[:,0], data_a[:,1], marker='x')
    plt.scatter(data_b[:,0], data_b[:,1], marker='*')
    plt.xlim([-8, 8])
    plt.ylim([-8, 8])
    plt.grid(linestyle='--')
    plt.show()

if __name__ == '__main__':
    sys.exit(Main());

