\documentclass[12pt,mathserif,aspectratio=169]{beamer}

\usepackage{booktabs}
\usepackage{parskip}
\usepackage{url}

\beamertemplatenavigationsymbolsempty

\setlength{\parskip}{\smallskipamount}
\setbeamertemplate{footline}[frame number]

\title{Introduction to Machine Learning}
\subtitle{2018 Seminar at VSCLab}

\author{Hengyang Zhao}
\institute{Deptartment of Electrical and Computer Engineering \\
           University of California, Riverside}

\date{February 13}

\begin{document}
\frame{\titlepage}

\begin{frame}
    \frametitle{Problem I: A Simple Machine Learning Example}
    Knowledge:
    \begin{table}
        \begin{tabular}{c|c|c}
            \toprule
            Input & Output & Hint (not part of the knowledge)\\
            \midrule
            $[1,2,3]$ & $[6, 7]$ & sum = 6\\
            $[-1,2,4]$ & $[5, 4]$ & sum = 5\\
            $[-1,-1,2]$ & $[0, -1]$ & sum = 0\\
            $[-5,6,1]$ & $[2, -3]$ & sum = 2\\
            \bottomrule
        \end{tabular}
    \end{table}

    Question: Given a new input $[10, 10, 10]$, what is the output?

    Answer: $O_1 \approx I_1 + I_2 + I_3$ and $O_2 \approx 2I_1 + I_2 + I_3$,
    so $[30, 40]$.
\end{frame}

\begin{frame}
    \frametitle{Problem II: An Image Recognition Example}
    \framesubtitle{A non-trivial machine learning application}

    Knowledge:
    \begin{table}
        \begin{tabular}{l|c|c|c|c|c}
            \toprule
            Image
            & \includegraphics[height=36pt]{images/cat1.jpg}
            & \includegraphics[height=36pt]{images/dog1.jpg}
            & \includegraphics[height=36pt]{images/dog2.jpg}
            & \includegraphics[height=36pt]{images/tree1.jpg}
            & \ldots\\
            \midrule
            Label & Cat & Dog & Dog & Tree & \ldots\\
            \bottomrule
        \end{tabular}
    \end{table}

    Question: \includegraphics[height=20pt]{images/cat2.jpg} = ?
              \includegraphics[height=20pt]{images/tree2.jpg} = ?
              \ldots

    The answer is obvious to humans.

\end{frame}

\begin{frame}

    \frametitle{Solution to Problem I}
    In Problem I, it is very easy to build a software to estimate the output
    \[
        \boldsymbol{\tilde y} = \boldsymbol f_\mathrm{I}(\boldsymbol x) =
        \boldsymbol A \boldsymbol x =\begin{bmatrix} 1&1&1\\2&1&1 \end{bmatrix}
            \boldsymbol x,
    \]
    where $\boldsymbol x$ denotes a $3\times1$ input vector,
    $\boldsymbol{\tilde y}$ denotes the $2\times1$ estimated output,
    and $\boldsymbol A$ is a $2\times3$ matrix which can be \emph{quickly}
    determined using \emph{least square method}.

    A good $\boldsymbol f_\mathrm{I}$ usually agrees well with the knowledge,
    and reasonably estimates for any input that's not seen in the knowledge.

\end{frame}

\begin{frame}
    \frametitle{Difference between Problems I and II}

    \begin{itemize}
        \item $\boldsymbol f_\mathrm{II}$ can be highly nonlinear and it will
            be very difficult to prescribe a mathematical form that's suitable
            for the problem.
        \item Dimension of $\boldsymbol x$ in Problem II is much higher than in
            I. For a $32\times32$ RGB888 image, $\boldsymbol x$ will be in size
            $24576\times1$. Knowledge base will also be much larger in Problem
            II than in Problem I.
    \end{itemize}

    Machine learning techniques fill the two gaps above, allowing us to
    numerically reconstruct the math represented by the given knowledge.

    Before we dive deeper, it's time to know some commonly used terms.
\end{frame}

\begin{frame}
    \frametitle{Machine Learning Terminologies}
    \framesubtitle{Model}

    \textbf{Model}: $\boldsymbol f_\mathrm{I}$ and $\boldsymbol f_\mathrm{II}$
    are called \emph{models}.

    Our objective is to acquire these models, which agree well with the
    knowledge, and give satisfactory estimations for new inputs.

    For example, to store the model $\boldsymbol f_\mathrm{I}$, we need to
    store the content of matrix $\boldsymbol A$. For $\boldsymbol
    f_\mathrm{II}$, we might need a much larger storage.
\end{frame}

\begin{frame}
    \frametitle{Machine Learning Terminologies}
    \framesubtitle{Training, validation, and testing}

    \textbf{Training}: The process we acquire the model.

    \textbf{Validate}: The process we evaluate the model using the knowledge
    base.

    \textbf{Testing}: The process we evaluate the model using something not
    seen in knowledge base.

    We also use these words to distinguish the data for the corresponding
    purpose.
    \begin{figure}
        \includegraphics{images/tvt.eps}
    \end{figure}

\end{frame}

\begin{frame}
    \frametitle{Machine Learning Terminologies}
    \framesubtitle{Supervised and unsupervised}
    \textbf{Supervised learning}: In a given knowledge base, all data are
    \emph{labeled}, which means each input is provided with its corresponding
    output, or each $\boldsymbol x$ has a paired $\boldsymbol y$.  So a
    training process can be \emph{supervised} by the \emph{labeled} data.

    Supervised learning has two sub-categories: \emph{regression} and
    \emph{classification}.

    \begin{block}{}
        \begin{minipage}{0.45\textwidth} \centering
            \begin{table}
                \begin{tabular}{c|c}
                    \toprule
                    $\boldsymbol x$ & $\boldsymbol y$\\
                    \midrule
                    $[1,2,3]$ & $[6, 7]$ \\
                    $[-1,2,4]$ & $[5, 4]$ \\
                    $[-1,-1,2]$ & $[0, -1]$ \\
                    $[-5,6,1]$ & $[2, -3]$ \\
                    \bottomrule
                \end{tabular}
                \caption{A regression example}
            \end{table}
        \end{minipage}
        \begin{minipage}{0.45\textwidth} \centering
            \begin{table}
                \begin{tabular}{c|c}
                    \toprule
                    $\boldsymbol x$ & $\boldsymbol y$\\
                    \midrule
                    $[1,2,3]$ & $[1, 0, 0]$ \\
                    $[1,2,4]$ & $[1, 0, 0]$ \\
                    $[-1,-1,2]$ & $[0, 1, 0]$ \\
                    $[-5,-3,-11]$ & $[0, 0, 1]$ \\
                    \bottomrule
                \end{tabular}
                \caption{A classification example}
            \end{table}
        \end{minipage}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Machine Learning Terminologies}
    \framesubtitle{Supervised and unsupervised}
    \textbf{Unsupervised learning}: Only $\boldsymbol x$'s are given. Usually
    we are asked to divide them into groups (\emph{clustering}), or find the
    items which have abnormal characteristics (\emph{outlier/anomaly
    detection}).

    \begin{block}{}
        \begin{minipage}{0.45\textwidth} \centering
            \begin{table}
                \begin{tabular}{c}
                    \toprule
                    $\boldsymbol x$\\
                    \midrule
                    $[2,4,6,88]$\\
                    $[2,-100,50,0]$\\
                    $[-1,3,95,45]$\\
                    $[7,5,11,-1]$\\
                    \bottomrule
                \end{tabular}
                \caption{All-even and all-odd vectors}
            \end{table}
        \end{minipage}
        \begin{minipage}{0.45\textwidth} \centering
            \begin{table}
                \begin{tabular}{c}
                    \toprule
                    $\boldsymbol x$\\
                    \midrule
                    $[1,2,3,10]$\\
                    $[1,2,4,5]$\\
                    $[9,10,20,30]$\\
                    $[4,5,\mathbf{7},\mathbf{6}]$\\
                    \bottomrule
                \end{tabular}
                \caption{Outlier in all-ascending vectors}
            \end{table}
        \end{minipage}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{From Least Squares Method to Real Machine Learning}
    \framesubtitle{Generalization from $\boldsymbol f_\text{I}$ to $\boldsymbol
    f_\text{II}$}
    \begin{itemize}
        \item In machine learning, we usually have much more complexed model
            than $\boldsymbol f = \boldsymbol A \boldsymbol x$ (which also
            counts as machine learning: linear regression).

        \item How do we come up with a mathematical form that can model those
            complexed applications (e.g. image recognition, voice recognition)?

        \item We will need an $\boldsymbol f$ that has a \emph{very long}
            expression!

        \item We have many different ways to construct $\boldsymbol f$:
            \begin{itemize}
                \item Simple ones: linear regression $\boldsymbol f=\boldsymbol A \boldsymbol x+\boldsymbol b$, logistic
                    regression $\boldsymbol f=\frac1{1+\exp(-(\boldsymbol A\boldsymbol x+\boldsymbol b))}$, etc.
                \item Complexed ones: artificial neural network (ANN), support
                    vector machine (SVM), hidden Markov model, etc.
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Introduction to Artificial Neural Network}
    \framesubtitle{Artificial neuron}

    \begin{figure}
        \includegraphics[height=1.2in]{images/neuron.eps}
    \end{figure}

    Math form:
    \[
        y=\mathcal S(\sum_{i=1}^{n}w_i x_i)=\mathcal S(\boldsymbol w \cdot
        \boldsymbol x)
    \]

\end{frame}

\begin{frame}
    \frametitle{Introduction to Artificial Neural Network}
    \framesubtitle{Activation function}
    Activation function $\mathcal S$ is a nonlinear function, for
    example: 
    \[
        \mathcal S(x)=\mathrm{sigmoid}(x)=\frac1{1+e^{-x}}
    \]
    \begin{figure}
        \includegraphics[width=0.8\textwidth]{images/sigmoid.eps}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Introduction to Artificial Neural Network}
    \framesubtitle{ANN structure}

    \begin{figure}
        \includegraphics[width=0.5\textwidth]{images/ann.eps}
    \end{figure}

    \[
        \boldsymbol f = \mathcal S (\boldsymbol W_{n} \mathcal S (\boldsymbol W_{n-1} \mathcal S(\cdots \mathcal S(\boldsymbol W_1 \boldsymbol x)\cdots)))
    \]
\end{frame}

\begin{frame}
    \frametitle{From Least Squares Method to Real Machine Learning}
    \framesubtitle{Generalization from least squares to model training}

    \begin{itemize}
        \item Question: What does least squares do?

        \item Answer:
            \begin{itemize}
                \item It finds $\boldsymbol W$ that minimizes $\Vert
                    \boldsymbol f_{\boldsymbol W}(\boldsymbol x) - \boldsymbol y\Vert$.

                \item It is both optimal (in sense of 2-norm), and fast
                    (non-iterative).

                \item But only for easy regressions!
            \end{itemize}

        \item For complexed models like ANN, we do not have a method that is
            either fast, or guarantees optimal solution.

        \item Therefore, a lot of \emph{iterative training methods} are
            proposed(such as the famous \emph{back-propagation} for ANN).
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Back-Propagation}
    \framesubtitle{A famous ANN training method based on gradient decent}

    \begin{itemize}
        \item It is iterative.
        \item It starts from a randomized, ``non-sense'' $\boldsymbol W$.
        \item On each step, it finds the direction $\Delta \boldsymbol W$ that
            improves the model, and then do $\boldsymbol W \leftarrow
            \boldsymbol W + \Delta \boldsymbol W$.

        \item The name ``back-propagation'' comes from how $\Delta \boldsymbol
            W$ is calculated.

        \item Technologies involved:
            \begin{itemize}
                \item Auto differential;
                \item Efficient matrix multiplication;
                \item Parallel computing;
                \item etc.
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Before We Start Using Software\ldots}

    \begin{itemize}
        \item A GREAT introduction to machine learning, by Andrew Ng: \url{https://www.coursera.org/learn/machine-learning}

        \item We need to know which method is better to be used to solve which
            kind of problem.

            \begin{itemize}
                \item So that we can ``prescribe'' the model $\boldsymbol f$.
                \item And let the software take care of the complicated training process.
            \end{itemize}
    \end{itemize}

    \bigskip
    \begin{figure}
        \includegraphics[height=0.4in]{images/logos/caffe2.png}
        \hspace{1em}
        \includegraphics[height=0.4in]{images/logos/keras.png}
        \hspace{1em}
        \includegraphics[height=0.4in]{images/logos/scikit-learn.png}
        \hspace{1em}
        \includegraphics[height=0.4in]{images/logos/spark-mllib.jpg}
        \hspace{1em}
        \includegraphics[height=0.4in]{images/logos/tensorflow.png}
        \hspace{1em}
        \includegraphics[height=0.4in]{images/logos/torch.png}
    \end{figure}

\end{frame}
\end{document}
