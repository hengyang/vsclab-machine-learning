\documentclass[12pt,mathserif,aspectratio=169]{beamer}

\usepackage{booktabs}
\usepackage{parskip}
\usepackage{url}
\usepackage{listings}

\beamertemplatenavigationsymbolsempty

\setlength{\parskip}{\smallskipamount}
\setbeamertemplate{footline}[frame number]

\lstset{
    basicstyle=\ttfamily,
    stringstyle=\color{red!30!black},
    commentstyle=\color{green!50!black},
    keywordstyle=\color{blue!80!black},
    identifierstyle=\color{yellow!20!purple},
    numbers=left,
    numberstyle=\color{black!60!white}\tiny,
    numbersep=8pt,
    stepnumber=1,
    basewidth=0.5em
}

\title{Introduction to \includegraphics[height=0.9em]{images/logos/tensorflow.png}}
\subtitle{2018 Seminar at VSCLab}

\author{Hengyang Zhao}
\institute{Deptartment of Electrical and Computer Engineering \\
           University of California, Riverside}

\date{February 13}

\begin{document}
\frame{\titlepage}

\begin{frame}
    \frametitle{A Simple Example from TensorFlow Playground}
    \framesubtitle{\url{http://playground.tensorflow.org}}

    \begin{figure}
        \includegraphics[width=\textwidth]{images/tf-playground.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Prepare Some Data}
    \framesubtitle{\texttt{scripts/simple-ann/gen-data.py}}
    \begin{figure}
        \begin{minipage}{0.45\textwidth} \centering
            \includegraphics[height=2.5in]{images/2class-data.eps}
        \end{minipage}
        \begin{minipage}{0.45\textwidth} \centering
            \begin{tabular}{cc|c}
                \toprule
                x & y & label\\
                \midrule
                -0.052 & 0.246 & 0.000 \\
                -0.113 & 5.978 & 1.000 \\
                0.511 & -1.619 & 0.000 \\
                -4.266 & -2.180 & 1.000\\
                -1.195 & -6.252 & 1.000\\
                -0.759 & 0.092 & 0.000 \\
                2.695 & 0.717 & 0.000  \\
                1.179 & -5.850 & 1.000 \\
                $\vdots$ & $\vdots$ & $\vdots$ \\
                \bottomrule
            \end{tabular}
        \end{minipage}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Read the Generated CSV File into Memory}
    \begin{figure}
        \includegraphics[width=0.8\textwidth]{images/jupyter-ann/import-read.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Separate of the Data: Training, Validation, and Testing}
    \framesubtitle{During training, we will use \emph{training} and
    \emph{validation}, but will NOT access \emph{testing} data}

    \begin{figure}
        \includegraphics[width=0.8\textwidth]{images/jupyter-ann/tvt-separation.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Build function $\boldsymbol f$}
    \framesubtitle{We also build a loss function for the trainer to have a goal}
    \begin{figure}
        \includegraphics[width=0.8\textwidth]{images/jupyter-ann/loss-function.png}
    \end{figure}

    Actual computation will NOT take place here.
\end{frame}

\begin{frame}
    \frametitle{Trainer}
    \begin{figure}
        \includegraphics[width=0.8\textwidth]{images/jupyter-ann/trainer.png}
    \end{figure}

    A trainer actually is an \emph{optimizer}.

    Learning rate is a very important parameter in machine learning. It affects
    convergence speed and learning depth.
\end{frame}

\begin{frame}
    \frametitle{Session}
    \framesubtitle{A context for actual computation}

    \begin{figure}
        \includegraphics[width=0.8\textwidth]{images/jupyter-ann/session.png}
    \end{figure}

    Actual TF computations happens in a \emph{session}, which may be backed by
    CPUs, GPUs, or TPUs (Tensor Processing Unit), or a heterogeneous computing
    cluster.

    By default, it utilizes all available computing devices. (For example a 16
    core CPU and 2 GPUs on newton.)
\end{frame}

\begin{frame}
    \frametitle{Weights Initialization}
    \begin{figure}
        \includegraphics[width=0.8\textwidth]{images/jupyter-ann/init-weight.png}
    \end{figure}

    Weights are required to be randomly initialized.

    Initialization is an actual operation. So we run it in the declared session.

    ** Any trivial-looking operation can be expensive in some cases.
\end{frame}

\begin{frame}
    \frametitle{Evaluation of the Raw Model}
    Because we now have a session, we can do some evaluation even before
    the model is trained.

    \begin{figure}
        \includegraphics[width=0.8\textwidth]{images/jupyter-ann/pre-train-eval.png}
    \end{figure}

    We can see that the model's output doesn't make sense and the loss is high.
\end{frame}

\begin{frame}
    \frametitle{Training}
    \begin{figure}
        \includegraphics[width=0.8\textwidth]{images/jupyter-ann/training.png}
    \end{figure}

    Including the loss, we also need to observe many other parameters when the
    training is ongoing. TensorBoard will help.

    In this case, validation loss is even lower than training, which is not
    common. This means the validation dataset happens to have less noise.
    Usually validation loss and training loss loss should be close.

    If validation loss $>$ training loss: over-fitting!
\end{frame}

\begin{frame}
    \frametitle{Visualize Decision Boundary}
    \begin{figure}
        \includegraphics[width=0.8\textwidth]{images/jupyter-ann/decision-boundary.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Evaluation of Trained Model}
    \begin{figure}
        \includegraphics[width=0.8\textwidth]{images/jupyter-ann/post-train-eval.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Use TensorFlow with GPU}
    \framesubtitle{Configuration}

    Now newton is ready for GPU computing.

    Anaconda is a good Python distribution and supports convenient TensorFlow
    setup.

    Anaconda does not require admin to install:
    \begin{itemize}
        \item Download and install Anaconda: \url{https://www.anaconda.com/download}

        \item \texttt{bash\$ conda install tensorflow-gpu}

        \item \texttt{python> import tensorflow}
    \end{itemize}

\end{frame}

\begin{frame}[fragile]
    \frametitle{Use TensorFlow with GPU}
    \framesubtitle{A trivial example}
    \begin{lstlisting}[language=Python]
    import tensorflow as tf

    with tf.device('/gpu:0'):
        a = tf.constant([1.0, 2.0], shape=[2, 1], name='a')
        b = tf.constant([-3.0, 4.0], shape=[1, 2], name='b')
    c = tf.matmul(a, b)

    sess = tf.Session()

    print(sess.run(c))
    \end{lstlisting}

    (Source: \url{https://www.tensorflow.org/programmers_guide/using_gpu})
\end{frame}

\end{document}
