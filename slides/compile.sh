#!/bin/sh

if type latexmk &>/dev/null; then
    latexmk -pdf
    exit
fi

if type pdflatex &>/dev/null; then
    for texfile in *.tex; do
        pdflatex "$texfile"
    done
    exit
fi

echo "No latexmk or pdflatex found on your system."
exit 1

