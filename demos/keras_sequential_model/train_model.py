import sys

if sys.version_info[0] < 3: raise Exception("Requires Python3")

import keras
import pandas

def CreateModel():
    model = keras.models.Sequential()

    # We just use dense (i.e. fully connected) layers
    model.add(keras.layers.Dense(units=10, activation='sigmoid', input_shape=(3, )))
    model.add(keras.layers.Dense(units=10, activation='sigmoid'))
    model.add(keras.layers.Dense(units=8, activation='sigmoid'))
    model.add(keras.layers.Dense(units=2, activation='linear'))

    return model

# This block will NOT run if this file is imported from another file
# It only runs if the file is directly executed
if __name__ == '__main__':

    # Pandas data frame
    # It's just like an scriptable MS Excel sheet
    # Note that we don't look at test_data.csv at all. This is only training.
    training_data = pandas.read_csv("training_data.csv")

    # Data can be extracted columnwise and convert to numpy arrays
    training_x = training_data[['a', 'b', 'c']].values
    training_y = training_data[['f', 'g']].values

    # The model structure is described in a separate function for clearance
    model = CreateModel()

    # Push model construction and optimization to the backend (tensorflow here)
    model.compile(loss='mean_squared_error', optimizer='adam')

    # This will issue the actual training
    # You will see decreasing loss(training loss) and val_loss(validation loss)
    model.fit(training_x, training_y,
              validation_split=0.2, # We use 20% data for validation
              epochs=1500,          # The total number of epochs (i.e. iterations)
              batch_size=50)        # Only use a small batch per epoch to train

    model.save("saved_model.h5")

