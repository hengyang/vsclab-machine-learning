Keras (TensorFlow Backend) Regression Demo
==========================================

Installation
------------

Recommended python: [Anaconda Python 3](https://www.anaconda.com/download)

Required packages: `pandas`, `keras` and `tensorflow`.

Note: For now please use keras version < 2.2. 2.1.6 is tested.

Demos
-----

1. Read and run `generate_dataset.py`
2. Read and run `train_model.py`
3. Read and run `test_model.py`
