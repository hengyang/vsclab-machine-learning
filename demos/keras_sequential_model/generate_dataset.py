import sys

if sys.version_info[0] < 3: raise Exception("Requires Python3")

import pandas
import numpy

# Modify here to have different amount of data generated
SIZE = 1000

# And here to determine how much will goto the training/testing set
TRAINING_RATIO = 0.85

# Just a shortcut for uniform distribution U[0, 1]
def U(): return numpy.random.uniform(low=0.0, high=1.0, size=SIZE)

# Randomized training input
a = U() * 2
b = U() * 5 + 1
c = U() * 10 - 5

# Training output, calculated by a deterministic rule using the input
f = a * 2 + b
g = a * c + 2 * b

# We use two separate files for training and testing data to make it easier
# to ensure NO TESTING DATA WILL PARTICIPATE TRAINING
training_file = 'training_data.csv'
testing_file = 'testing_data.csv'

# Build a easy-to-use, Excel-like pandas data frame
data_frame = pandas.DataFrame({'a': a, 'b': b, 'c': c, 'f': f, 'g': g})

# Split the data frame and then write out
training_testing_cut_point = int(SIZE * TRAINING_RATIO)

data_frame[:training_testing_cut_point].to_csv(training_file)
print("Wrote file %s." % training_file)

data_frame[training_testing_cut_point:].to_csv(testing_file)
print("Wrote file %s." % testing_file)

