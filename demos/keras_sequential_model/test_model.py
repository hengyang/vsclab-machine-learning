import sys

if sys.version_info[0] < 3: raise Exception("Requires Python3")

import keras
import numpy
import pandas

if __name__ == '__main__':

    model = keras.models.load_model("saved_model.h5")

    # From generate_dataset.py, expecting:
    # f = 2a + b
    # g = ac + 2b
    print(model.predict(numpy.array([[1, 1, 1]]))) # Expects [[3, 3]]
    print(model.predict(numpy.array([[0, 1, 1]]))) # Expects [[1, 2]]

    # Expects [[1, 2]] too, but ANN is not good at exprapolation. In training
    # it only saw c in [-5, 5] so...
    print(model.predict(numpy.array([[0, 1, 10]])))

    testing_data = pandas.read_csv("testing_data.csv")
    testing_x = testing_data[['a', 'b', 'c']].values
    testing_y = testing_data[['f', 'g']].values

    testing_loss = model.test_on_batch(testing_x, testing_y)
    print("Testing loss is %g." % testing_loss)
    
